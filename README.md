**Vim for Python, Lua, PHP, bash...**\
\
F1 - White chars (Show/Hide)\
F2 - Numbers (Show/Hide)\
F3 - Horizontal Line Cursor (Show/Hide)\
\
F4 - Explorer Open/Close\
F5 - Paste Toogle \
F7 - Search hl\
\
Ctrl + / add/del comment (visual mode)\
Shift + Tab toggle between windows\
Ctrl + arrow keys change tab \
Shift + arrow keys resize current window\
\
**Add tmux config**\
Control - a
